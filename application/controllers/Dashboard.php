<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Dashboard_db','',TRUE);
	}

	public function index(){}

	public function showDashboard(){
		$data_msg['message'] = "";

		if($this->session->userdata('error_msg') != ""){
			foreach($initial_array as $v => $key){
				$data_msg[$v] = $this->session->userdata($v);
			}
			$this->session->unset_userdata($initial_array);
			$data_msg['error_msg'] = $this->session->userdata('error_msg');
			$this->session->unset_userdata('error_msg');
		}

		if($this->session->userdata('success_msg') != ""){
			$data_msg['success_msg'] = $this->session->userdata('success_msg');
			$this->session->unset_userdata('success_msg');
		}

		$allBlogs = $this->Dashboard_db->getall();
		$data_msg['all_blogs'] = $allBlogs->result_array();

		$this->load->view('dashboard_view',$data_msg);
	}


	public function insertBlog(){
		$data_msg['message'] = "";

		$initial_array = array(
			'title' => '',
			'description' => '',
			'image' => '',
			'tags' => '',
			'error_msg' => ''
		);

		if($this->session->userdata('error_msg') != ""){
			foreach($initial_array as $v => $key){
				$data_msg[$v] = $this->session->userdata($v);
			}
			$this->session->unset_userdata($initial_array);
			$data_msg['error_msg'] = $this->session->userdata('error_msg');
			$this->session->unset_userdata('error_msg');
		}

		if($this->session->userdata('success_msg') != ""){
			$data_msg['success_msg'] = $this->session->userdata('success_msg');
			$this->session->unset_userdata('success_msg');
		}

		if($this->input->post('create_btn_blog')){

			$error_msg = "";

			$create_btn_blog = trim(strip_tags($this->input->post('create_btn_blog')));
			$title = trim(strip_tags($this->input->post('title')));
			$description = trim(strip_tags($this->input->post('description')));
			$tags = $this->input->post('tags');
			$image = $_FILES['image']['name'];

			if ($title == ""){
				$error_msg = "Please provide title";
			} elseif ($description == ""){
				$error_msg = "Please provide description";
			} elseif (count($tags) == 0){
				$error_msg = "Please provide tags";
			} elseif ($image == ""){
				$error_msg = "Please provide image";
			} 

			if($error_msg != ""){
				$sess_array = array();
				foreach ($initial_array as $key => $v) {
					if(isset($$key)){
						$sess_array[$key] = $$key;
					}
				}
				$this->session->Set_userdata($sess_array);
				redirect(base_url().'insert-blog');
				exit;
			} else {

				if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != ''){
					$image = $_FILES['image']['name'];
					$image = str_replace(' ','',$image);
					$image = rand() . $image;
					$fileTmpLoc = $_FILES['image']['tmp_name'];
					$path = $_SERVER['DOCUMENT_ROOT'].'/blog_management/uploads/';
					$pathAndName = $path . $image;
					$moveResult = move_uploaded_file($fileTmpLoc, $pathAndName);
				}

				$data = array(
					'title' => $title,
					'description' => $description,
					'image' => $image,
					'tags' => implode(',', $tags),
					'created_date' => date('Y-m-d'),
					'updated_date' => date('Y-m-d'),
					'created_by' => $_SESSION['user_id'],
					'updated_by' => $_SESSION['user_id'],
					'status' => '1'
				);

				$last_id = $this->Dashboard_db->insert_data($data);

				$this->session->set_userdata('success_msg','Blog created!');

				if($last_id != ''){
					redirect(base_url().'insert-blog');
					exit;
				} else{
					redirect(base_url().'insert-blog');
					exit;
				}
			}
		}
		$this->load->view('insert_blog_view',$data_msg);
	}


	public function deleteBlog($id){
		$data_msg['message'] = "";

		$this->Dashboard_db->deleteBlog($id);

		redirect(base_url().'dashboard');
	}


	public function editBlog($id){

		$initial_array = array(
			'title' => '',
			'description' => '',
			'image' => '',
			'tags' => '',
			'error_msg' => ''
		);

		if($this->session->userdata('error_msg') != ""){
			foreach($initial_array as $v => $key){
				$data_msg[$v] = $this->session->userdata($v);
			}
			$this->session->unset_userdata($initial_array);
			$data_msg['error_msg'] = $this->session->userdata('error_msg');
			$this->session->unset_userdata('error_msg');
		}

		if($this->session->userdata('success_msg') != ""){
			$data_msg['success_msg'] = $this->session->userdata('success_msg');
			$this->session->unset_userdata('success_msg');
		}

		$allBlogs = $this->Dashboard_db->getbyid($id);
		$data_msg['details'] = $allBlogs->row_array();

		if($this->input->post('edit_btn_blog')){

			$error_msg = "";

			$edit_btn_blog = trim(strip_tags($this->input->post('edit_btn_blog')));
			$title = trim(strip_tags($this->input->post('title')));
			$description = trim(strip_tags($this->input->post('description')));
			$pre_image = $this->input->post('pre_image');
			$tags = $this->input->post('tags');

			if ($title == ""){
				$error_msg = "Please provide title";
			} elseif ($description == ""){
				$error_msg = "Please provide description";
			} elseif (count($tags) == 0){
				$error_msg = "Please provide tags";
			} 

			if($error_msg != ""){
				$sess_array = array();
				foreach ($initial_array as $key => $v) {
					if(isset($$key)){
						$sess_array[$key] = $$key;
					}
				}
				$this->session->Set_userdata($sess_array);
				redirect(base_url().'edit-blog');
				exit;
			} else {

				if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != ''){
					$image = $_FILES['image']['name'];
					$image = str_replace(' ','',$image);
					$image = rand() . $image;
					$fileTmpLoc = $_FILES['image']['tmp_name'];
					$path = $_SERVER['DOCUMENT_ROOT'].'/blog_management/uploads/';
					$pathAndName = $path . $image;
					$moveResult = move_uploaded_file($fileTmpLoc, $pathAndName);
				} else {
					$image = $pre_image;
				}

				$data = array(
					'title' => $title,
					'description' => $description,
					'image' => $image,
					'tags' => implode(',', $tags),
					'created_date' => date('Y-m-d'),
					'updated_date' => date('Y-m-d'),
					'created_by' => $_SESSION['user_id'],
					'updated_by' => $_SESSION['user_id'],
					'status' => '1'
				);

				$this->Dashboard_db->update_data($data,$id);

				$this->session->set_userdata('success_msg','Blog updated!');

				redirect(base_url().'edit-blog/'.$id);
				exit;
				
			}
		}
		$this->load->view('edit_view',$data_msg);
	}
}
