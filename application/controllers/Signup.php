<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Signup_db','',TRUE);
	}

	public function index(){}

	public function registration(){

		$data_msg['message'] = "";

		$initial_array = array(
			'name' => '',
			'email' => '',
			'password' => '',
			'error_msg' => ''
		);

		if($this->session->userdata('error_msg') != ""){
			foreach($initial_array as $v => $key){
				$data_msg[$v] = $this->session->userdata($v);
			}
			$this->session->unset_userdata($initial_array);
			$data_msg['error_msg'] = $this->session->userdata('error_msg');
			$this->session->unset_userdata('error_msg');
		}

		if($this->session->userdata('success_msg') != ""){
			$data_msg['success_msg'] = $this->session->userdata('success_msg');
			$this->session->unset_userdata('success_msg');
		}

		if($this->input->post('create_btn_user')){

			$error_msg = "";

			$create_btn_user = trim(strip_tags($this->input->post('create_btn_user')));
			$name = trim(strip_tags($this->input->post('name')));
			$email = trim(strip_tags($this->input->post('email')));
			$password = trim(strip_tags($this->input->post('password')));
			$confirm_password = trim(strip_tags($this->input->post('confirm_password')));

			if ($name == ""){
				$error_msg = "Please provide name";
			} elseif ($email == ""){
				$error_msg = "Please provide email";
			} elseif ($this->Signup_db->check_email_exist($email)->num_rows() > 0){
				$error_msg = "Email already exists";
			} elseif ($password == ""){
				$error_msg = "Please provide password";
			} elseif ($confirm_password == ""){
				$error_msg = "Please re-enter your password";
			} elseif ($password != $confirm_password){
				$error_msg = "Password mismatch";
			}

			if($error_msg != ""){
				$sess_array = array();
				foreach ($initial_array as $key => $v) {
					if(isset($$key)){
						$sess_array[$key] = $$key;
					}
				}
				$this->session->Set_userdata($sess_array);
				redirect(base_url().'registration');
				exit;
			} else {
				$data = array(
					'name' => $name,
					'email' => $email,
					'password' => md5($password),
					'created_date' => date('Y-m-d'),
					'status' => '1'
				);

				$last_id = $this->Signup_db->insert_data($data);

				$this->session->set_userdata('success_msg','New registration successful!');

				if($last_id != ''){
					redirect(base_url().'registration');
					exit;
				} else{
					redirect(base_url().'registration');
					exit;
				}
			}
		}
		$this->load->view('signup_view',$data_msg);
	}
}
