<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Login_db','',TRUE);
		$this->load->library('encryption');
	}

	public function index(){}

	public function loginUser(){

		$initial_array = array(
			'email' => '',
			'error_msg' => ''
		);

		$data_msg['message'] = "";

		if($this->session->userdata('error_msg') != ""){
			foreach($initial_array as $v => $key){
				$data_msg[$v] = $this->session->userdata($v);
			}
			$this->session->unset_userdata($initial_array);
			$data_msg['error_msg'] = $this->session->userdata('error_msg');
			$this->session->unset_userdata('error_msg');
		}

		if($this->session->userdata('success_msg') != ""){
			$data_msg['success_msg'] = $this->session->userdata('success_msg');
			$this->session->unset_userdata('success_msg');
		}

		if($this->input->post('login_btn_user')){

			$error_msg = "";

			$login_btn_user = trim(strip_tags($this->input->post('login_btn_user')));
			$email = trim(strip_tags($this->input->post('email')));
			$password = trim(strip_tags($this->input->post('password')));

			if ($email == ""){
				$error_msg = "Please provide email";
			} elseif ($password == ""){
				$error_msg = "Please provide password";
			} else {
				$get_userinformation = $this->Login_db->matchuser($email,md5($password));

				if($get_userinformation->num_rows() == 0){
					$error_msg = "Email or password is incorrect!";
				} else {
					$user_information = $get_userinformation->row_array();
					$this->session->set_userdata(array('user_id' => $user_information['user_id'], 'name' => $user_information['name'], 'email' => $user_information['email']));

					$this->session->unset_userdata('error_msg');
					redirect(base_url().'dashboard');
					exit();
				}
			}

			if($error_msg != ""){
				$sess_array = array('email' => $email,
					'error_msg' => $error_msg
				);
				$this->session->set_userdata($sess_array);
				redirect(base_url().'log-in');
				exit;
			} 
		}

		$this->load->view('login_view',$data_msg);
	}


	public function logoutUser(){
		if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != '0'){
			$this->session->sess_destroy();
		}

		redirect(base_url('dashboard'));
	}
}
