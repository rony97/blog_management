<?php 

class Signup_db extends CI_Model{
	
	public function check_email_exist($email){
		$this->db->select('email');
		$this->db->from('bm_users');
		$this->db->where('email',$email);
		return $this->db->get();
	}

	public function insert_data($data){
		$this->db->insert('bm_users',$data);
		return $this->db->insert_id();
	}
}

?>