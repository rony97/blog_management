<?php 

class Dashboard_db extends CI_Model{
	
	public function matchuser($email,$password){
		$this->db->select('user_id,email,name');
		$this->db->from('bm_users');
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		$this->db->where('status','1');
		$query = $this->db->get();
		return $query;
	}

	public function insert_data($data){
		$this->db->insert('bm_blogs',$data);
		return $this->db->insert_id();
	}

	public function deleteBlog($id){
		$this->db->where('id',$id);
		$this->db->delete('bm_blogs');
		return true;
	}


	public function update_data($data,$id){
		$this->db->where('id',$id);
		$this->db->update('bm_blogs',$data);
		return true;
	}


	public function getall(){
		$this->db->select('*');
		$this->db->from('bm_blogs');
		$this->db->where('status','1');
		$query = $this->db->get();
		return $query;
	}


	public function getbyid($id){
		$this->db->select('*');
		$this->db->from('bm_blogs');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query;
	}
}

?>