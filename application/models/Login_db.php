<?php 

class Login_db extends CI_Model{
	
	public function matchuser($email,$password){
		$this->db->select('user_id,email,name');
		$this->db->from('bm_users');
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		$this->db->where('status','1');
		$query = $this->db->get();
		return $query;
	}

	public function insert_data($data){
		$this->db->insert('bm_users',$data);
		return $this->db->insert_id();
	}
}

?>