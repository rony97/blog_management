<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard - Blog Management</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
  /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
  .row.content {height: 1500px}

  /* Set gray background color and 100% height */
  .sidenav {
    background-color: #f1f1f1;
    height: 100%;
  }

  /* Set black background color, white text and some padding */
  footer {
    background-color: #555;
    color: white;
    padding: 15px;
  }

  /* On small screens, set height to 'auto' for sidenav and grid */
  @media screen and (max-width: 767px) {
    .sidenav {
      height: auto;
      padding: 15px;
    }
    .row.content {height: auto;} 
  }
</style>
</head>
<body>

  <div class="container-fluid">
    <div class="row content">
      <div class="col-sm-3 sidenav">
        <h4>DASHBOARD</h4>
        <ul class="nav nav-pills nav-stacked">
          <li class="active"><a href="<?php echo base_url().'dashboard'; ?>">Home</a></li>
          <?php if(!isset($_SESSION['user_id']) or $_SESSION['user_id'] == '') { ?>
            <li><a href="<?php echo base_url().'registration'; ?>">Signup</a></li>
            <li><a href="<?php echo base_url().'log-in'; ?>">Login</a></li>
          <?php } else { ?>
            <li><a href="<?php echo base_url().'log-out'; ?>">Logout</a></li>
          <?php } ?>
        </ul><br>
      </div>

      <div class="col-sm-9">
        <?php if(isset($_SESSION['name']) && $_SESSION['name'] != '') { ?>
          <h4><small>Logged in as : <?php echo $_SESSION['name']; ?></small></h4>
        <?php } ?>
        <?php if(isset($error_msg) && $error_msg != "") { ?>
          <span class="alert alert-warning">
            <?php echo $error_msg; ?>
          </span>
        <?php } ?>

        <?php if(isset($success_msg) && $success_msg != "") { ?>
          <span class="alert alert-success">
            <?php echo $success_msg; ?>
          </span>
        <?php } ?>

        <hr>

        <div class="container">
          <h2>LIST</h2>
          <ul class="nav nav-pills nav-stacked">
            <?php if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != '') { ?>
              <li><a href="<?php echo base_url('insert-blog'); ?>">Insert</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url('dashboard'); ?>">Reload</a></li>
          </ul><br>      
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Image</th>
                <th>Title</th>
                <th>Description</th>
                <th>Tags</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php if(isset($all_blogs) && $all_blogs != ''){
                foreach ($all_blogs as $key => $value) { ?>

                  <tr>
                    <td> <img style="height: 100px; width:100px" src="<?php echo base_url().'uploads/'.$value['image']; ?>"> </td>
                    <td> <?php if(isset($value['title']) && $value['title'] != '') echo $value['title']; ?> </td>
                    <td> <?php if(isset($value['description']) && $value['description'] != '') echo substr($value['description'],0,100).'...'; ?> </td>
                    <td> <?php if(isset($value['tags']) && $value['tags'] != '') echo $value['tags']; ?> </td>
                    <?php if(isset($value['created_by']) && $value['created_by'] != '' && isset($_SESSION['user_id']) && $_SESSION['user_id'] != '' && $value['created_by'] == $_SESSION['user_id']) { ?>
                      <td> <a href="<?php echo base_url().'edit-blog/'.$value['id']; ?>" class="link-secondary">Edit</a> ||  <a href="<?php echo base_url().'delete-blog/'.$value['id']; ?>" onclick="return confirm('Are you sure?')" class="link-secondary">Delete</a></td>
                    <?php } ?>
                  </tr>

                <?php }
              } ?>
            </tbody>
          </table>
        </div>


      </div>
    </div>
  </div>

</body>
</html>
