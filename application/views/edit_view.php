<!DOCTYPE html>
<html lang="en">
<head>
  <title>Insert Blog - Blog Management</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
  /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
  .row.content {height: 1500px}

  /* Set gray background color and 100% height */
  .sidenav {
    background-color: #f1f1f1;
    height: 100%;
  }

  /* Set black background color, white text and some padding */
  footer {
    background-color: #555;
    color: white;
    padding: 15px;
  }

  /* On small screens, set height to 'auto' for sidenav and grid */
  @media screen and (max-width: 767px) {
    .sidenav {
      height: auto;
      padding: 15px;
    }
    .row.content {height: auto;} 
  }
</style>
</head>
<body>

  <div class="container-fluid">
    <div class="row content">
      <div class="col-sm-3 sidenav">
        <h4>DASHBOARD</h4>
        <ul class="nav nav-pills nav-stacked">
          <li class="active"><a href="<?php echo base_url().'dashboard'; ?>">Home</a></li>
          <?php if(!isset($_SESSION['user_id']) or $_SESSION['user_id'] == '') { ?>
          <li><a href="<?php echo base_url().'registration'; ?>">Signup</a></li>
          <li><a href="<?php echo base_url().'log-in'; ?>">Login</a></li>
        <?php } else { ?>
          <li><a href="<?php echo base_url().'log-out'; ?>">Logout</a></li>
        <?php } ?>
        </ul><br>
      </div>

      <div class="col-sm-9">
        <?php if(isset($error_msg) && $error_msg != "") { ?>
        <span class="alert alert-warning">
          <?php echo $error_msg; ?>
        </span>
      <?php } ?>

        <?php if(isset($success_msg) && $success_msg != "") {?>

        <span class="alert alert-success">
          <?php echo $success_msg; ?>
        </span>
        <?php } ?>
        <hr>



        <div class="container">
          <h2>EDIT BLOG</h2>

          <ul class="nav nav-pills nav-stacked">
            <?php if(isset($_SESSION['user_id']) or $_SESSION['user_id'] != '') { ?>
              <li><a href="<?php echo base_url('dashboard'); ?>">List</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url('edit-blog/'.$details['id']); ?>">Reload</a></li>
          </ul>
          
          <form method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="usr">Title*:</label>
              <input type="text" class="form-control" id="title" name="title" autocomplete="off" maxlength="255" value="<?php if(isset($details['title']) && $details['title'] != "") echo $details['title']; ?>" required="required">
            </div>

            <div class="form-group">
              <label for="usr">Description*:</label>
              <textarea class="form-control" id="description" name="description" maxlength="65535" autocomplete="off" required="required"><?php if(isset($details['description']) && $details['description'] != "") echo $details['description']; ?></textarea>
            </div>

            <div class="form-group">
              <label for="pwd">Image:</label>
              <input type="file" class="form-control" id="image" name="image" autocomplete="off" value="">
              <input type="hidden" class="form-control" id="pre_image" name="pre_image" value="<?php if(isset($details['image']) && $details['image'] != "") echo $details['image']; ?>">
            </div>


            <div class="form-group">
              <label for="pwd">Image Preview:</label>
              <?php if(isset($details['image']) && $details['image'] != '') { ?>
              <img style="height: 100px; width:100px" src="<?php echo base_url().'uploads/'.$details['image']; ?>">
            <?php } ?>
            </div>

            <div id="topic_file">
              <div id="file_sec0" class="r_parent">

                <!-- <a id="delete_row" href="javascript:void();" onclick="return del(this,'file_sec0')">Remove tag </a> -->

            <div class="form-group">
              <label for="pwd">Tags:</label>
              <input type="text" class="form-control" id="tags0" name="tags[]" autocomplete="off" value="<?php if(isset($details['tags']) && $details['tags'] != "") echo $details['tags']; ?>" required="required">
            </div>

              </div>
            </div>


            <div class="form-group">
              <a href="javascript:void(0)" id = "add" class="link-secondary" onclick="return add_more(0);">Add More</a>
            </div>

            <div class="form-group">
              <input type="submit" class="btn btn-info" id="edit_btn_blog" name="edit_btn_blog" value="Update">
            </div>
          </form>
        </div>


      </div>
    </div>
  </div>

  <script>
    
    function add_more(counter) {
      var pretags = $('#tags'+counter).val();
      var ctags = '';
      if(pretags != ''){
        ctags = parseInt(pretags) + 1;
      }

      counter = parseInt(counter) + 1;
      var html = '';

      html += '<div id="file_sec'+counter+'" class="r_parent">';

      html += '<a id="delete_row" href="javascript:void();" onclick="return del(this,"file_sec'+counter+'")">Remove tag </a>';

      html += '<div class="form-group">';
      html += '<label for="pwd">Tags:</label>';
      html += '<input type="text" class="form-control" id="tags'+counter+'" name="tags[]" autocomplete="off" value="" required="required">';
      html += '</div>';
      
      html += '</div>';

      jQuery('#topic_file').append(html);
      jQuery('#add_more').attr('onclick','add_more('+counter+')');

    }

  </script>


  <script>
    
    function del(athis,sec_id) {
      $(athis).parents('#'+sec_id).remove();
      return true;
    }

  </script>

</body>
</html>
