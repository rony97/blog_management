<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login - Blog Management</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
  /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
  .row.content {height: 1500px}

  /* Set gray background color and 100% height */
  .sidenav {
    background-color: #f1f1f1;
    height: 100%;
  }

  /* Set black background color, white text and some padding */
  footer {
    background-color: #555;
    color: white;
    padding: 15px;
  }

  /* On small screens, set height to 'auto' for sidenav and grid */
  @media screen and (max-width: 767px) {
    .sidenav {
      height: auto;
      padding: 15px;
    }
    .row.content {height: auto;} 
  }
</style>
</head>
<body>

  <div class="container-fluid">
    <div class="row content">
      <div class="col-sm-3 sidenav">
        <h4>DASHBOARD</h4>
        <ul class="nav nav-pills nav-stacked">
          <li class="active"><a href="<?php echo base_url().'dashboard'; ?>">Home</a></li>
          <?php if(!isset($_SESSION['user_id']) or $_SESSION['user_id'] == '') { ?>
          <li><a href="<?php echo base_url().'registration'; ?>">Signup</a></li>
          <li><a href="<?php echo base_url().'log-in'; ?>">Login</a></li>
        <?php } else { ?>
          <li><a href="<?php echo base_url().'log-out'; ?>">Logout</a></li>
        <?php } ?>
        </ul><br>
      </div>

      <div class="col-sm-9">
        <?php if(isset($error_msg) && $error_msg != "") { ?>
        <span class="alert alert-warning">
          <?php echo $error_msg; ?>
        </span>
      <?php } ?>

        <?php if(isset($success_msg) && $success_msg != "") { ?>
        <span class="alert alert-success">
          <?php echo $success_msg; ?>
        </span>
        <?php } ?>
        <hr>



        <div class="container">
          <h2>LOGIN USER</h2>
          <form method="post" enctype="multipart/form-data">

            <div class="form-group">
              <label for="usr">Email id*:</label>
              <input type="email" class="form-control" id="email" name="email" autocomplete="off" value="<?php if(isset($email) && $email != "") echo $email; ?>" required="required">
            </div>

            <div class="form-group">
              <label for="pwd">Password:</label>
              <input type="password" class="form-control" id="password" name="password" autocomplete="off" value="" required="required">
            </div>

            <div class="form-group">
              <input type="submit" class="btn btn-info" id="login_btn_user" name="login_btn_user" value="Login">
            </div>
          </form>
        </div>


      </div>
    </div>
  </div>

</body>
</html>
