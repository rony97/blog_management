-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2021 at 11:11 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bm_blog_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `bm_blogs`
--

CREATE TABLE `bm_blogs` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '''0''=>Inacitve, ''1''=> Active, ''2''=> Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bm_blogs`
--

INSERT INTO `bm_blogs` (`id`, `title`, `description`, `image`, `tags`, `created_date`, `updated_date`, `created_by`, `updated_by`, `status`) VALUES
(2, 'test blog', 'test blog', '', 'tag 2,tag 3', '2021-06-12', '2021-06-12', 1, 1, '1'),
(3, 'test', 'tass', '542189856365468.jpg', '123', '2021-06-12', '2021-06-12', 1, 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `bm_users`
--

CREATE TABLE `bm_users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_date` date NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '''0''=> Active, ''1'' => Inactive, ''2'' => Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bm_users`
--

INSERT INTO `bm_users` (`user_id`, `name`, `email`, `password`, `created_date`, `status`) VALUES
(1, 'Suvodeep Gupta', 'gupta.suvodeep@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2021-06-12', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bm_blogs`
--
ALTER TABLE `bm_blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bm_users`
--
ALTER TABLE `bm_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bm_blogs`
--
ALTER TABLE `bm_blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bm_users`
--
ALTER TABLE `bm_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
